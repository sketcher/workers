import { createDisposableWorker } from './functions'
import QueryableWorker from './queryableWorker';

export default {
  install: (app, options) => {
    app.config.globalProperties.$worker = (() => {
      let wrapper = new QueryableWorker()
      return wrapper;
    })()
  }
}
