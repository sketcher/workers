/**
 * Generate blob from passed in function
 * @param {Function} script 
 * @returns {Worker} worker
 */
export const createDisposableWorker = (script = function () { }) =>
    new Worker(URL.createObjectURL(new Blob(
        [script.toString().replace(/^function .+\{?|\}$/g, '')],
        { type: 'text/javascript' }
    )))
