/**
 * Will be run by the worker in a separated multithread.
 * Avoid using ecmascript that is too new when calling with an in-line blob
 * These won't get caught/transformed by webpacks
 */
export function example() {

    onmessage = function ({ data: { id, payload } }) {
        let msg = { id, err: false, payload: null }

        try {
            msg.payload = 'hello there[worker/server]'
        }
        catch (err) {
            msg.err = err
        }
        finally {
            postMessage(msg)
        }
    }
}

export function exampleError() {
    onmessage = function ({ data: { id, payload } }) {
        postMessage( "string".push([],{}))
    }
}

export function exampleHeavy() {

    onmessage = function ({ data: { id, payload } }) {
        let msg = { id, err: false, payload: null }

        try {
            let mark = performance.now()
            let arr = new Array(10_000_000).fill(Math.random())
            let res = []
            for(let i = 0; i < arr.length; i++){
                res.push(arr[i])
            }

            msg.payload = { msg:'done[worker]', found, len:res.length, duration: (performance.now() - mark) / 1000}
        }
        catch (err) {
            msg.err = err
        }
        finally {
            postMessage(msg)
        }
    }
}