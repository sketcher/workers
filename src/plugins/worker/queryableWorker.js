import { createDisposableWorker } from "./functions"

const resolves = {}
const rejects = {}
let globalMsgId = 0

class QueryableWorker {
    constructor() {

    }

    setWorker(script) {
        if (!(script instanceof Function))
            throw new Error('Argument was not a function')
        this.worker = createDisposableWorker(script)
        this.worker.onmessage = this.handleMsg
        this.worker.onmessageerror = this.handleMessageError
        this.worker.onerror = this.handleError
    }

    oche(payload) {
        if (!(this.worker instanceof Worker))
            throw new Error(`Webworker has not been initialized. Use $worker.${this.setWorker.name}(<script>)`)
        return this.sendMsg(payload, this.worker)
    }

    sendMsg(payload, worker) {
        const msg = { id: globalMsgId++, payload }
        return new Promise(function (resolve, reject) {
            resolves[msg.id] = resolve
            rejects[msg.id] = reject
            worker.postMessage(msg)
        })
    }

    handleError(err) {
        console.error(err)
    }

    handleMessageError(err) {
        console.error(err)
    }

    handleMsg({ data: { id, err, payload } }) {

        const resolve = resolves[id]
        const reject = rejects[id]

        if (err) {
            reject(err)
        }

        resolve(payload)

        // purge used callbacks
        delete resolves[id]
        delete rejects[id]
    }
}

export default QueryableWorker