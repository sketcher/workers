import { createApp } from 'vue'
import worker from './plugins/worker/'
import App from './App.vue'

const app = createApp(App)
app.use(worker, {})
app.mount('#app')
